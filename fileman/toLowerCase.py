import argparse
from .helpers import writeStringToFile


def toLowerCase(filename: str) -> str:
    """
    This function takes in a file and returns it as a string in
    lowercase.
    """
    return open(filename, "r").read().lower()


def main():
    parser = argparse.ArgumentParser(
        description="Convert file to all lowercase"
    )
    parser.add_argument("file", help="file to read in")
    parser.add_argument(
        "-o", "--output", required=False, help="file to output to"
    )
    args = parser.parse_args()
    file = toLowerCase(args.file)

    if args.output:
        writeStringToFile(file, args.output)
    else:
        print(file)


if __name__ == "__main__":
    main()
