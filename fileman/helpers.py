def writeListToFile(file, filename):
    output = open(filename, "w")
    column = map(lambda line: line + "\n", file)
    output.writelines(column)
    output.close()


def writeStringToFile(file, fileName):
    output = open(fileName, "w")
    output.write(file)
    output.close()
