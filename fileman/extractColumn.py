import argparse
import re


from .helpers import writeListToFile


def extractColumn(filename: str, colNum: int) -> list:
    """
    This function takes in a file with columns and rows and returns the
    specified column as a list.
    """
    file = open(filename, "r").read().lower().splitlines()
    column = [re.split("\\s+", x)[colNum] for x in file]

    return column


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Extract a column from a file"
    )
    parser.add_argument("file", help="file to read in")
    parser.add_argument(
        "columnNumber", type=int, help="which column to extract"
    )
    parser.add_argument(
        "-o", "--output", required=False, help="file to output to"
    )
    args = parser.parse_args()
    column = extractColumn(args.file, args.columnNumber)

    if args.output:
        writeListToFile(column, args.output)
    else:
        print(*column)
